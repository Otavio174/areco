object dfmCadastroProduto: TdfmCadastroProduto
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Cadastro Produto'
  ClientHeight = 447
  ClientWidth = 723
  Color = clBtnFace
  Constraints.MaxHeight = 486
  Constraints.MaxWidth = 739
  Constraints.MinHeight = 486
  Constraints.MinWidth = 739
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PanelPrincipal: TPanel
    Left = 0
    Top = 0
    Width = 723
    Height = 447
    Align = alClient
    ParentBackground = False
    TabOrder = 0
    object Label2: TLabel
      Left = 10
      Top = 73
      Width = 48
      Height = 16
      Caption = 'C'#243'digo :'
    end
    object Label1: TLabel
      Left = 146
      Top = 75
      Width = 42
      Height = 16
      Caption = 'Nome :'
    end
    object Label9: TLabel
      Left = 224
      Top = 416
      Width = 37
      Height = 16
      Caption = 'Label9'
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 97
      Width = 721
      Height = 348
      ActivePage = tbsInformacaoProduto
      Align = alCustom
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 6
      OnChange = PageControl1Change
      object tbsInformacaoProduto: TTabSheet
        Caption = 'Informa'#231#245'es Gerais'
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 713
          Height = 317
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label3: TLabel
            Left = 338
            Top = 21
            Width = 107
            Height = 16
            Caption = 'C'#243'digo de Barras :'
          end
          object Label4: TLabel
            Left = 6
            Top = 21
            Width = 70
            Height = 16
            Caption = 'Refer'#234'ncia :'
          end
          object Label10: TLabel
            Left = 5
            Top = 221
            Width = 82
            Height = 16
            Caption = 'Observa'#231#245'es :'
          end
          object Label16: TLabel
            Left = 5
            Top = 50
            Width = 74
            Height = 16
            Caption = 'Localiza'#231#227'o :'
          end
          object edtCodigoBarras: TEdit
            Left = 451
            Top = 18
            Width = 252
            Height = 24
            TabOrder = 1
          end
          object edtReferencia: TEdit
            Left = 85
            Top = 18
            Width = 236
            Height = 24
            TabOrder = 0
          end
          object GroupBox2: TGroupBox
            Left = 5
            Top = 121
            Width = 404
            Height = 91
            Caption = ' Informa'#231#245'es de Estoque '
            TabOrder = 3
            object Label7: TLabel
              Left = 33
              Top = 29
              Width = 38
              Height = 16
              Caption = 'Atual :'
            end
            object Label8: TLabel
              Left = 216
              Top = 29
              Width = 50
              Height = 16
              Caption = 'M'#237'nimo :'
            end
            object Label11: TLabel
              Left = 22
              Top = 58
              Width = 53
              Height = 16
              Caption = 'M'#225'ximo :'
            end
            object edtEstoqueAtual: TEdit
              Left = 77
              Top = 26
              Width = 130
              Height = 24
              TabOrder = 0
              Text = '0,00'
              OnExit = edtEstoqueAtualExit
            end
            object edtEstoqueMin: TEdit
              Left = 268
              Top = 26
              Width = 130
              Height = 24
              TabOrder = 1
              Text = '0,00'
              OnExit = edtEstoqueMinExit
            end
            object edtEstoqueMax: TEdit
              Left = 77
              Top = 55
              Width = 130
              Height = 24
              TabOrder = 2
              Text = '0,00'
              OnExit = edtEstoqueMaxExit
            end
          end
          object memobs: TMemo
            Left = 102
            Top = 218
            Width = 601
            Height = 39
            TabOrder = 5
          end
          object btnOk: TButton
            Left = 623
            Top = 263
            Width = 80
            Height = 50
            Cursor = crHandPoint
            Caption = 'Ok'
            ImageAlignment = iaTop
            ImageIndex = 16
            ImageMargins.Top = 4
            Images = dtmGeral.ImgListButoesSecundarios
            TabOrder = 7
            OnClick = btnOkClick
          end
          object GroupBox1: TGroupBox
            Left = 415
            Top = 78
            Width = 290
            Height = 134
            Caption = 'Valores'
            TabOrder = 4
            object Label12: TLabel
              Left = 46
              Top = 24
              Width = 41
              Height = 16
              Caption = 'Custo :'
            end
            object Label13: TLabel
              Left = 8
              Top = 51
              Width = 79
              Height = 16
              Caption = 'Custo M'#233'dio :'
            end
            object Label14: TLabel
              Left = 6
              Top = 107
              Width = 81
              Height = 16
              Caption = 'Pre'#231'o Venda :'
            end
            object Label15: TLabel
              Left = 21
              Top = 78
              Width = 66
              Height = 16
              Caption = 'Lucro (%) :'
            end
            object edtCusto: TEdit
              Left = 93
              Top = 21
              Width = 172
              Height = 24
              TabOrder = 0
              Text = '0,00'
              OnExit = edtCustoExit
            end
            object edtCustoMedio: TEdit
              Left = 93
              Top = 48
              Width = 172
              Height = 24
              TabOrder = 1
              Text = '0,00'
              OnExit = edtCustoMedioExit
            end
            object edtPrecoVenda: TEdit
              Left = 93
              Top = 100
              Width = 172
              Height = 24
              TabOrder = 3
              Text = '0,00'
              OnExit = edtPrecoVendaExit
            end
            object edtLucro: TEdit
              Left = 93
              Top = 75
              Width = 172
              Height = 24
              TabOrder = 2
              Text = '0,00'
              OnExit = edtLucroExit
            end
          end
          object edtLocalizacao: TEdit
            Left = 84
            Top = 47
            Width = 236
            Height = 24
            TabOrder = 2
          end
          object cbxControlarEstoque: TCheckBox
            Left = 12
            Top = 270
            Width = 139
            Height = 17
            Caption = 'Controlar Estoque'
            TabOrder = 6
          end
          object btnCancelar: TButton
            Left = 537
            Top = 263
            Width = 80
            Height = 50
            Cursor = crHandPoint
            Caption = 'Cancelar'
            ImageAlignment = iaTop
            ImageIndex = 15
            ImageMargins.Top = 4
            Images = dtmGeral.ImgListButoesSecundarios
            TabOrder = 8
            OnClick = btnCancelarClick
          end
        end
      end
    end
    object edtCodigo: TEdit
      Left = 61
      Top = 70
      Width = 79
      Height = 24
      ReadOnly = True
      TabOrder = 3
    end
    object edtNome: TEdit
      Left = 195
      Top = 70
      Width = 453
      Height = 24
      TabOrder = 4
    end
    object cbxAtivo: TCheckBox
      Left = 660
      Top = 74
      Width = 68
      Height = 17
      Caption = 'Ativo'
      TabOrder = 5
    end
    object btnIncluir: TButton
      Left = 10
      Top = 2
      Width = 80
      Height = 50
      Cursor = crHandPoint
      Hint = 'Adicionar'
      Caption = 'Adicionar'
      ImageAlignment = iaTop
      ImageIndex = 2
      ImageMargins.Top = 4
      Images = dtmGeral.ImgListButoesSecundarios
      TabOrder = 0
      OnClick = btnIncluirClick
    end
    object btnAlterar: TButton
      Left = 89
      Top = 2
      Width = 80
      Height = 50
      Cursor = crHandPoint
      Caption = 'Alterar'
      ImageAlignment = iaTop
      ImageIndex = 12
      ImageMargins.Top = 4
      Images = dtmGeral.ImgListButoesSecundarios
      TabOrder = 1
      OnClick = btnAlterarClick
    end
    object btnExcluir: TButton
      Left = 168
      Top = 2
      Width = 80
      Height = 50
      Cursor = crHandPoint
      Caption = 'Excluir'
      ImageAlignment = iaTop
      ImageIndex = 1
      ImageMargins.Top = 4
      Images = dtmGeral.ImgListButoesSecundarios
      TabOrder = 2
      OnClick = btnExcluirClick
    end
  end
end
