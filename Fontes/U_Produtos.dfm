object dfmProdutos: TdfmProdutos
  Left = 0
  Top = 0
  Caption = 'Produtos'
  ClientHeight = 515
  ClientWidth = 843
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object panel1: TPanel
    Left = 0
    Top = 0
    Width = 843
    Height = 515
    Align = alClient
    Caption = 'panel1'
    TabOrder = 0
    DesignSize = (
      843
      515)
    object cxGrid1: TcxGrid
      Left = 10
      Top = 64
      Width = 830
      Height = 327
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsrProdutos
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1ID_PRO: TcxGridDBColumn
          Caption = 'C'#243'digo'
          DataBinding.FieldName = 'ID_PRO'
        end
        object cxGrid1DBTableView1NOME_PRO: TcxGridDBColumn
          Caption = 'Nome'
          DataBinding.FieldName = 'NOME_PRO'
          Width = 246
        end
        object cxGrid1DBTableView1CUSTO: TcxGridDBColumn
          Caption = 'Custo'
          DataBinding.FieldName = 'CUSTO'
        end
        object cxGrid1DBTableView1CUSTOMEDIO: TcxGridDBColumn
          Caption = 'Custo M'#233'dio'
          DataBinding.FieldName = 'CUSTOMEDIO'
          Width = 98
        end
        object cxGrid1DBTableView1PRECOVENDA: TcxGridDBColumn
          Caption = 'Pre'#231'o de Venda'
          DataBinding.FieldName = 'PRECOVENDA'
          Width = 102
        end
        object cxGrid1DBTableView1LUCRO: TcxGridDBColumn
          Caption = 'Lucro'
          DataBinding.FieldName = 'LUCRO'
          Width = 58
        end
        object cxGrid1DBTableView1ESTOQATUAL: TcxGridDBColumn
          Caption = 'Estoque Atual'
          DataBinding.FieldName = 'ESTOQATUAL'
          Width = 98
        end
        object cxGrid1DBTableView1ESTOQMINIMO: TcxGridDBColumn
          Caption = 'Estoque M'#237'nimo'
          DataBinding.FieldName = 'ESTOQMINIMO'
          Width = 109
        end
        object cxGrid1DBTableView1ESTOQMAXIMO: TcxGridDBColumn
          Caption = 'Estoque M'#225'ximo'
          DataBinding.FieldName = 'ESTOQMAXIMO'
          Width = 111
        end
        object cxGrid1DBTableView1OBS: TcxGridDBColumn
          Caption = 'Observa'#231#227'o'
          DataBinding.FieldName = 'OBS'
          Width = 338
        end
        object cxGrid1DBTableView1ATIVO: TcxGridDBColumn
          DataBinding.FieldName = 'ATIVO'
          Visible = False
        end
        object cxGrid1DBTableView1REFERENCIA: TcxGridDBColumn
          Caption = 'Refer'#234'ncia'
          DataBinding.FieldName = 'REFERENCIA'
        end
        object cxGrid1DBTableView1DT_INCLUSAO: TcxGridDBColumn
          DataBinding.FieldName = 'DT_INCLUSAO'
          Visible = False
        end
        object cxGrid1DBTableView1LOCALIZACAO: TcxGridDBColumn
          Caption = 'Localiza'#231#227'o'
          DataBinding.FieldName = 'LOCALIZACAO'
        end
        object cxGrid1DBTableView1CONTROLA_ESTQ: TcxGridDBColumn
          DataBinding.FieldName = 'CONTROLA_ESTQ'
          Visible = False
        end
        object cxGrid1DBTableView1COD_BARRAS: TcxGridDBColumn
          Caption = 'C'#243'digo de Barras'
          DataBinding.FieldName = 'COD_BARRAS'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object btnDetalhesPro: TButton
      Left = 5
      Top = 8
      Width = 80
      Height = 50
      Cursor = crHandPoint
      Hint = 'Adicionar'
      Caption = 'DETALHES'
      HotImageIndex = 6
      ImageAlignment = iaTop
      ImageIndex = 6
      ImageMargins.Top = 4
      Images = dtmGeral.ImgListButoesSecundarios
      TabOrder = 1
      OnClick = btnDetalhesProClick
    end
    object btnExcluirPro: TButton
      Left = 85
      Top = 8
      Width = 80
      Height = 50
      Cursor = crHandPoint
      Caption = 'Excluir'
      ImageAlignment = iaTop
      ImageIndex = 1
      ImageMargins.Top = 4
      Images = dtmGeral.ImgListButoesSecundarios
      TabOrder = 2
      OnClick = btnExcluirProClick
    end
    object GroupBox1: TGroupBox
      Left = 5
      Top = 392
      Width = 835
      Height = 109
      Align = alCustom
      Anchors = [akLeft, akBottom]
      Caption = ' Pesquisas '
      TabOrder = 3
      object Label3: TLabel
        Left = 40
        Top = 34
        Width = 42
        Height = 16
        Caption = 'Nome :'
      end
      object Label1: TLabel
        Left = 12
        Top = 64
        Width = 70
        Height = 16
        Caption = 'Refer'#234'ncia :'
      end
      object Label2: TLabel
        Left = 370
        Top = 34
        Width = 107
        Height = 16
        Caption = 'C'#243'digo de Barras :'
      end
      object Label4: TLabel
        Left = 403
        Top = 64
        Width = 74
        Height = 16
        Caption = 'Localiza'#231#227'o :'
      end
      object edtNome: TEdit
        Left = 89
        Top = 30
        Width = 239
        Height = 24
        TabOrder = 0
      end
      object btnPesquisarPro: TButton
        Left = 749
        Top = 32
        Width = 80
        Height = 50
        Cursor = crHandPoint
        Hint = 'Adicionar'
        Align = alCustom
        Caption = 'Pesquisar'
        HotImageIndex = 6
        ImageAlignment = iaTop
        ImageIndex = 6
        ImageMargins.Top = 4
        Images = dtmGeral.ImgListButoesSecundarios
        TabOrder = 1
        OnClick = btnPesquisarProClick
      end
      object edtReferencia: TEdit
        Left = 89
        Top = 60
        Width = 239
        Height = 24
        TabOrder = 2
      end
      object edtcodigoBarras: TEdit
        Left = 483
        Top = 30
        Width = 214
        Height = 24
        TabOrder = 3
      end
      object edtLocalizacao: TEdit
        Left = 483
        Top = 60
        Width = 214
        Height = 24
        TabOrder = 4
      end
    end
    object btnIncluirPro: TButton
      Left = 165
      Top = 8
      Width = 80
      Height = 50
      Cursor = crHandPoint
      Caption = 'Incluir'
      ImageAlignment = iaTop
      ImageIndex = 2
      ImageMargins.Top = 4
      Images = dtmGeral.ImgListButoesSecundarios
      TabOrder = 4
      OnClick = btnIncluirProClick
    end
  end
  object dsrProdutos: TDataSource
    DataSet = fdqProdutos
    Left = 464
    Top = 9
  end
  object fdqProdutos: TFDQuery
    Connection = dtmGeral.conexao
    SQL.Strings = (
      'select * from produto where Ativo = "S"')
    Left = 392
    Top = 9
    object fdqProdutosID_PRO: TIntegerField
      FieldName = 'ID_PRO'
      Origin = 'ID_PRO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object fdqProdutosNOME_PRO: TStringField
      FieldName = 'NOME_PRO'
      Origin = 'NOME_PRO'
      Size = 100
    end
    object fdqProdutosCUSTO: TFloatField
      FieldName = 'CUSTO'
      Origin = 'CUSTO'
      DisplayFormat = 'R$#0.00'
    end
    object fdqProdutosCUSTOMEDIO: TFloatField
      FieldName = 'CUSTOMEDIO'
      Origin = 'CUSTOMEDIO'
      DisplayFormat = 'R$#0.00'
    end
    object fdqProdutosPRECOVENDA: TFloatField
      FieldName = 'PRECOVENDA'
      Origin = 'PRECOVENDA'
      DisplayFormat = 'R$#0.00'
    end
    object fdqProdutosLUCRO: TBCDField
      FieldName = 'LUCRO'
      Origin = 'LUCRO'
      DisplayFormat = '#0.00'
      Precision = 6
      Size = 2
    end
    object fdqProdutosESTOQATUAL: TBCDField
      FieldName = 'ESTOQATUAL'
      Origin = 'ESTOQATUAL'
      DisplayFormat = '#0.00'
      Precision = 10
    end
    object fdqProdutosESTOQMINIMO: TBCDField
      FieldName = 'ESTOQMINIMO'
      Origin = 'ESTOQMINIMO'
      DisplayFormat = '#0.00'
      Precision = 10
      Size = 2
    end
    object fdqProdutosESTOQMAXIMO: TBCDField
      FieldName = 'ESTOQMAXIMO'
      Origin = 'ESTOQMAXIMO'
      DisplayFormat = '#0.00'
      Precision = 10
      Size = 2
    end
    object fdqProdutosOBS: TStringField
      FieldName = 'OBS'
      Origin = 'OBS'
      Size = 500
    end
    object fdqProdutosATIVO: TStringField
      FieldName = 'ATIVO'
      Origin = 'ATIVO'
      Size = 1
    end
    object fdqProdutosREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Origin = 'REFERENCIA'
    end
    object fdqProdutosDT_INCLUSAO: TDateTimeField
      FieldName = 'DT_INCLUSAO'
      Origin = 'DT_INCLUSAO'
    end
    object fdqProdutosLOCALIZACAO: TStringField
      FieldName = 'LOCALIZACAO'
      Origin = 'LOCALIZACAO'
    end
    object fdqProdutosCONTROLA_ESTQ: TStringField
      FieldName = 'CONTROLA_ESTQ'
      Origin = 'CONTROLA_ESTQ'
      Size = 1
    end
    object fdqProdutosCOD_BARRAS: TStringField
      FieldName = 'COD_BARRAS'
      Origin = 'COD_BARRAS'
      Size = 30
    end
  end
end
