unit U_Produtos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.StdCtrls, Vcl.Mask;

type
  TdfmProdutos = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsrProdutos: TDataSource;
    fdqProdutos: TFDQuery;
    btnDetalhesPro: TButton;
    panel1: TPanel;
    btnExcluirPro: TButton;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    edtNome: TEdit;
    btnPesquisarPro: TButton;
    fdqProdutosID_PRO: TIntegerField;
    fdqProdutosNOME_PRO: TStringField;
    fdqProdutosCUSTO: TFloatField;
    fdqProdutosCUSTOMEDIO: TFloatField;
    fdqProdutosPRECOVENDA: TFloatField;
    fdqProdutosLUCRO: TBCDField;
    fdqProdutosESTOQATUAL: TBCDField;
    fdqProdutosESTOQMINIMO: TBCDField;
    fdqProdutosESTOQMAXIMO: TBCDField;
    fdqProdutosOBS: TStringField;
    fdqProdutosATIVO: TStringField;
    fdqProdutosREFERENCIA: TStringField;
    fdqProdutosDT_INCLUSAO: TDateTimeField;
    fdqProdutosLOCALIZACAO: TStringField;
    fdqProdutosCONTROLA_ESTQ: TStringField;
    fdqProdutosCOD_BARRAS: TStringField;
    cxGrid1DBTableView1ID_PRO: TcxGridDBColumn;
    cxGrid1DBTableView1NOME_PRO: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTO: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOMEDIO: TcxGridDBColumn;
    cxGrid1DBTableView1PRECOVENDA: TcxGridDBColumn;
    cxGrid1DBTableView1LUCRO: TcxGridDBColumn;
    cxGrid1DBTableView1ESTOQATUAL: TcxGridDBColumn;
    cxGrid1DBTableView1ESTOQMINIMO: TcxGridDBColumn;
    cxGrid1DBTableView1ESTOQMAXIMO: TcxGridDBColumn;
    cxGrid1DBTableView1OBS: TcxGridDBColumn;
    cxGrid1DBTableView1ATIVO: TcxGridDBColumn;
    cxGrid1DBTableView1REFERENCIA: TcxGridDBColumn;
    cxGrid1DBTableView1DT_INCLUSAO: TcxGridDBColumn;
    cxGrid1DBTableView1LOCALIZACAO: TcxGridDBColumn;
    cxGrid1DBTableView1CONTROLA_ESTQ: TcxGridDBColumn;
    cxGrid1DBTableView1COD_BARRAS: TcxGridDBColumn;
    Label1: TLabel;
    edtReferencia: TEdit;
    edtcodigoBarras: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    edtLocalizacao: TEdit;
    btnIncluirPro: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDetalhesProClick(Sender: TObject);
    procedure btnExcluirProClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnPesquisarProClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnIncluirProClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dfmProdutos: TdfmProdutos;

implementation

{$R *.dfm}

uses dtm, untRotinas, U_CadastroProduto;

procedure TdfmProdutos.btnDetalhesProClick(Sender: TObject);
begin

  if fdqProdutos.RecordCount < 1 then  begin
    Informacao('Nenhum registro selecionado!','AVISO');
    exit;
  end;

  try
    dfmCadastroProduto:= TdfmCadastroProduto.Create(Application);
    dfmCadastroProduto.edtCodigo.Text:= fdqProdutosID_PRO.AsString;
    dfmCadastroProduto.edtNome.Text:=  fdqProdutosNOME_PRO.AsString;
    dfmCadastroProduto.edtCodigoBarras.Text:= fdqProdutosCOD_BARRAS.AsString;
    dfmCadastroProduto.edtReferencia.Text:=  fdqProdutosREFERENCIA.AsString;
    dfmCadastroProduto.edtEstoqueAtual.Text:= fdqProdutosESTOQATUAL.AsString;
    dfmCadastroProduto.edtEstoqueMin.Text:=  fdqProdutosESTOQMINIMO.AsString;
    dfmCadastroProduto.edtEstoqueMax.Text:=  fdqProdutosESTOQMAXIMO.AsString;
    dfmCadastroProduto.memobs.Text:=  fdqProdutosOBS.AsString;
    dfmCadastroProduto.edtCusto.Text:= fdqProdutosCUSTO.AsString;
    dfmCadastroProduto.edtCustoMedio.Text:= fdqProdutosCUSTOMEDIO.AsString;
    dfmCadastroProduto.edtPrecoVenda.Text:= fdqProdutosPRECOVENDA.AsString;
    dfmCadastroProduto.edtLucro.Text:= fdqProdutosLUCRO.AsString;
    dfmCadastroProduto.edtLocalizacao.Text:= fdqProdutosLOCALIZACAO.AsString;


    if fdqProdutosATIVO.AsString = 'S' then
      dfmCadastroProduto.cbxAtivo.Checked:= true
    else
      dfmCadastroProduto.cbxAtivo.Checked:= false;

    dfmCadastroProduto.AtivaCamposProduto('C');
    dfmCadastroProduto.ShowModal;

  finally

  end;

end;



procedure TdfmProdutos.btnExcluirProClick(Sender: TObject);
var
  comando : String;
begin
  if fdqProdutos.RecordCount = 0 then begin
    exit;
  end;

  Comando := 'delete from produto where ID_PRO = ' + fdqProdutosID_PRO.AsString;

  ExecutaComando(Comando);

  fdqProdutos.Refresh;
end;



procedure TdfmProdutos.btnIncluirProClick(Sender: TObject);
begin
  dfmCadastroProduto:= TdfmCadastroProduto.Create(Application);
  dfmCadastroProduto.AtivaCamposProduto('I');
  dfmCadastroProduto.ShowModal;
end;



procedure TdfmProdutos.btnPesquisarProClick(Sender: TObject);
var
  sSQL : String;
begin
  sSQL:= ' select * from produto where ID_PRO > 0 ';

  if edtNome.Text <> '' then
    sSQL:= sSQL + ' and NOME_PRO like "%' + edtNome.Text + '%"';

  if edtReferencia.Text <> '' then
    sSQL:= sSQL + ' and REFERENCIA like "%' + edtReferencia.Text + '%" ';

  if edtcodigoBarras.Text <> '' then
    sSQL:= sSQL + ' and COD_BARRAS like "%' + edtcodigoBarras.Text + '%" ';

  if edtLocalizacao.Text <> '' then
    sSQL:= sSQL + ' and LOCALIZACAO like "%' + edtLocalizacao.Text + '%" ';


  fdqProdutos.Close;
  fdqProdutos.SQL.Text:= sSQL;
  fdqProdutos.Open;
end;



procedure TdfmProdutos.btnSairClick(Sender: TObject);
begin
  Close;
end;



procedure TdfmProdutos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fdqProdutos.Close;

end;



procedure TdfmProdutos.FormShow(Sender: TObject);
begin
  fdqProdutos.Active:= false;
  fdqProdutos.SQL.Text:= 'select * from produto';
  fdqProdutos.Active:= true;
end;

end.
