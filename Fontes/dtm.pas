unit dtm;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, Data.DB,
  FireDAC.Comp.Client, FireDAC.Phys.MySQL, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  IniFiles, Dialogs, Forms, Vcl.ImgList, Vcl.Controls, cxStyles, cxClasses,
  cxGraphics, Datasnap.DBClient, SimpleDS, Data.DBXMySQL, Data.SqlExpr;

type
  TdtmGeral = class(TDataModule)
    conexao: TFDConnection;
    MySQL_Link: TFDPhysMySQLDriverLink;
    WaitCursor: TFDGUIxWaitCursor;
    ImgListButoesSecundarios: TcxImageList;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     usuarioLogado : integer;
     Host, DataBase: String;
  end;

var
  dtmGeral: TdtmGeral;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses untrotinas;

procedure TdtmGeral.DataModuleCreate(Sender: TObject);
begin

  try
    // Adicionar par�metros ao componente de conex�o

    Conexao.Params.Clear;
    Conexao.Params.Add('DriverID=MySQL');
    Conexao.Params.Add('Database= bdteste');
    Conexao.Params.Add('User_Name=root');
    Conexao.Params.Add('Password=android');
    Conexao.Params.Add('Server=127.0.0.1');

    Conexao.Connected := true;

  except
    Informacao('N�o foi poss�vel conectar ao banco de dados', 'Aviso');
    raise;
    Halt(1);
  end;

end;

end.
