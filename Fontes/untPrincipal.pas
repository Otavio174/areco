unit untPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, dxGDIPlusClasses,
  Vcl.ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.StdCtrls,
  cxButtons, Vcl.Mask, Vcl.Imaging.jpeg;

type
  TdfmPrincipal = class(TForm)
    Image1: TImage;
    btnEntrar: TcxButton;
    procedure btnEntrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dfmPrincipal: TdfmPrincipal;

implementation

{$R *.dfm}

uses U_Produtos;

procedure TdfmPrincipal.btnEntrarClick(Sender: TObject);
begin
  try
    dfmProdutos:= TdfmProdutos.Create(Application);
    dfmProdutos.ShowModal;
  finally
    FreeAndNil(dfmProdutos);
  end;
end;

end.
