unit untRotinas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxContainer, Vcl.StdCtrls, cxTextEdit,
  cxMemo, simpleDs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Comp.Client, Data.FMTBcd, Data.SqlExpr,
  cxMaskEdit, cxDropDownEdit, ComCtrls, cxGridExportLink;

type

  TItemCombo = Class(TList)
    public
      nKey : integer;
  end;

function CriaQuery(sSQL : string): TFDQuery;
procedure Informacao(const Mensagem, Titulo : string);
function GetCod(tabela, campo : string) : integer;
function ExecutaComando(sComando : string) : boolean;
function valorSQL(nValor : Double): string;
function stringtofloatZero(sValor: string) : double;

implementation

uses dtm;



function CriaQuery(sSQL: string): TFDQuery;
var Query : TFDQuery;
begin

  try

    Query := TFDQuery.Create(nil);
    Query.Connection := dtmGeral.conexao;
    Query.SQL.Append(sSQL);
    Query.Open;

    if Query.RecordCount > 0 then
      Result := Query

    else
      Result := nil;

  except
    Result := nil;

  end;

end;



procedure Informacao(const Mensagem, Titulo : string);
begin

  Application.MessageBox(Pchar(Mensagem), Pchar(Titulo), mb_ok or mb_IconInformation);

end;



function GetCod(tabela, campo: string): integer;
var Query : TFDQuery;
begin

  try

    Query := CriaQuery('select ' + campo + ' from ' + tabela + ' order by ' + campo + ' desc limit 1');

    if Query <> nil then begin

      result := query.FieldByName(campo).AsInteger + 1;

    end
    else begin

      result := 1;

    end;

  except

  end;

end;



function ExecutaComando(sComando : string) : boolean;
var Qry : TFDQuery;
begin
  try

    result := true;

    Qry := TFDQuery.Create(nil);
    Qry.Connection := dtmGeral.conexao;

    Qry.SQL.Clear;
    Qry.SQL.Add(SComando);
    Qry.ExecSQL;
    Qry.Close;

  except

    result := false;

  end;

end;



function valorSQL(nValor : Double): string;
var sValorRetorno,sValor : string;
    i : integer;
begin
  sValorRetorno:= '';
  sValor := floattostr(nValor);

  for i:= 1 to length(trim(sValor)) do
    if copy(sValor,i,1) = ',' then
      sValorRetorno:= sValorRetorno + '.'
    else
      sValorRetorno:= sValorRetorno + copy(sValor,i,1);

  result:= sValorRetorno;
end;




function stringtofloatZero(sValor: string) : double;
var nRetorno : double;
begin
  try
     nRetorno:= strtofloat(sValor)
  except
     nRetorno:= 0;
  end;

  Result := nRetorno;
end;

end.
