program Teste_Avaliacao;

uses
  Vcl.Forms,
  untPrincipal in 'Fontes\untPrincipal.pas' {dfmPrincipal},
  dtm in 'Fontes\dtm.pas' {dtmGeral: TDataModule},
  untRotinas in 'Fontes\untRotinas.pas' {$R *.res},
  U_Produtos in 'Fontes\U_Produtos.pas' {dfmProdutos},
  U_CadastroProduto in 'Fontes\U_CadastroProduto.pas' {dfmCadastroProduto};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdtmGeral, dtmGeral);
  Application.CreateForm(TdfmPrincipal, dfmPrincipal);
  Application.Run;
end.
